/*********************************************************************
 This is an example for our nRF52 based Bluefruit LE modules

 Pick one up today in the adafruit shop!

 Adafruit invests time and resources providing this open source code,
 please support Adafruit and open-source hardware by purchasing
 products from Adafruit!

 MIT license, check LICENSE for more information
 All text above, and the splash screen below must be included in
 any redistribution
*********************************************************************/

/* This sketch show how to use BLEClientService and BLEClientCharacteristic
 * to implement a custom client that is used to talk with Gatt server on
 * peripheral.
 *
 * Note: you will need another feather52 running peripheral/custom_HRM sketch
 * to test with.
 */

#include "Wire.h"
#include <bluefruit.h>
#include <LIS3MDL.h>
#define TCAADDR 0x70


//Multiplexer
void tcaselect(uint8_t i) {
  if (i > 7) return;
 
  Wire.beginTransmission(TCAADDR);
  Wire.write(1 << i);
  Wire.endTransmission();  
}
//
//Kodama
byte fullData[28] ;

bool hasHandShaked = false;
bool shouldStream = false;
unsigned long startTime;
unsigned long RoundTripTime;
unsigned long waitTime;

//array for the controller cobra
byte cobraData[21];
bool isCobraConnected = false;
const byte size = 5;


//Magnetometers
LIS3MDL mag1;
LIS3MDL mag2;
LIS3MDL mag3;
LIS3MDL mag4;
LIS3MDL mag5;

double m1x, m1y, m1z;
double m2x, m2y, m2z;
double m3x, m3y, m3z;
double m4x, m4y, m4z;
double m5x, m5y, m5z;

float averageM1X;
float averageM1Y;
float averageM1Z;

float averageM2X;
float averageM2Y;
float averageM2Z;

float averageM3X;
float averageM3Y;
float averageM3Z;

float averageM4X;
float averageM4Y;
float averageM4Z;

float averageM5X;
float averageM5Y;
float averageM5Z;

bool isFirst = true;
bool calibrated = false;
//String mgData;

int maxvalsindex[5];

float maxlistval;
int maxindex;


float emaX = 0;
float emaY = 0;
float emaZ = 0;

float emaEmaX = 0;
float emaEmaY = 0;
float emaEmaZ = 0;

float alpha = 0.15;

bool starterbool = true;

float newX = 0;
float newY = 0;
float newZ = 0;

int16_t newXint, newYint, newZint;


double posweight1x = -72;
double posweight2x = -72;
double posweight3x = 0;
double posweight4x = 72;
double posweight5x = 72;

int posweight1y = -72;
int posweight2y = 72;
int posweight3y = 0;
int posweight4y = -72;
int posweight5y = 72;

float WeightPositionx;
float WeightPositiony;
float WeightPositionz;

float WeightPositionxfirst;
float WeightPositionyfirst;
float WeightPositionzfirst;


    float exponent1;
    double parameter1;
    float exponent2;
    double parameter2;
    float exponent3;
    double parameter3;
    float exponent4;
    double parameter4;
    float exponent5;
    double parameter5;

float zdist1;
float zdist2;
float zdist3;
float zdist4;
float zdist5;
            
byte endOfData = 127;

float xPrevious;
float yPrevious;
float zPrevious;

byte led1 = 11;
byte led2 = 7;
byte led3 = 15;

int ledTimer = 0;

static const unsigned long REFRESH_INTERVAL = 30; // ms
static unsigned long lastRefreshTime = 0;


//end Kodama variables



/* HRM Service Definitions
 * Heart Rate Monitor Service:  0x180D
 * Heart Rate Measurement Char: 0x2A37 (Mandatory)
 * Body Sensor Location Char:   0x2A38 (Optional)
 */

 static uint8_t xAddress[] = {0xE6,0x19,0,0,0x6,0x1};

BLEClientService        hrms(0xf000);
BLEClientCharacteristic hrmc(0xf001);
BLEClientCharacteristic bslc(0xf002); // not sure

//BLEClientService        hrms(UUID16_SVC_HEART_RATE);
//BLEClientCharacteristic hrmc(UUID16_CHR_HEART_RATE_MEASUREMENT);
//BLEClientCharacteristic bslc(UUID16_CHR_BODY_SENSOR_LOCATION);

void setup()
{
// Serial.begin(9600);
    
//kodama
 pinMode(led1,OUTPUT);
  pinMode(led2,OUTPUT);
  pinMode(led3,OUTPUT);

  digitalWrite(led1, HIGH);
   Wire.begin();
 Wire.setClock(400000L);

 //Serial.begin(115200);
 Serial.begin(19200);
  Serial.setTimeout(10);
        calibrateDevice();  

//init the array
   for(int i = 1; i<20; i++){
     cobraData[i] = 0x0;
   }

//end kodama

  
  //Serial.begin(9600);
 

 // Serial.println("NRF52 breakout connect to Cobra");
 // Serial.println("--------------------------------------\n");

  // Initialize Bluefruit with maximum connections as Peripheral = 0, Central = 1
  // SRAM usage required by SoftDevice will increase dramatically with number of connections
  Bluefruit.begin(0, 1);

  Bluefruit.setName("XRmouse central");

  // Initialize HRM client
  hrms.begin();

  // Initialize client characteristics of HRM.
  // Note: Client Char will be added to the last service that is begin()ed.
  bslc.begin();

  // set up callback for receiving measurement
  hrmc.setNotifyCallback(hrm_notify_callback);
  hrmc.begin();

  // Increase Blink rate to different from PrPh advertising mode
  Bluefruit.setConnLedInterval(10);

  // Callbacks for Central
  Bluefruit.Central.setDisconnectCallback(disconnect_callback);
  Bluefruit.Central.setConnectCallback(connect_callback);

  /* Start Central Scanning
   * - Enable auto scan if disconnected
   * - Interval = 100 ms, window = 80 ms
   * - Don't use active scan
   * - Filter only accept HRM service
   * - Start(timeout) with timeout = 0 will scan forever (until connected)
   */
  Bluefruit.Scanner.setRxCallback(scan_callback);
  Bluefruit.Scanner.restartOnDisconnect(true);
  Bluefruit.Scanner.setInterval(160, 80); // in unit of 0.625 ms
  //Bluefruit.Scanner.filterUuid(xAddress);
  Bluefruit.Scanner.useActiveScan(true);
  Bluefruit.Scanner.start(0);                   // // 0 = Don't stop scanning after n seconds


}


void calibrateDevice(){
  
  
   tcaselect(2);
    delay(100);
    mag1.init();
    mag1.enableDefault();
    
    tcaselect(3);
    delay(100);
    mag2.init();
    mag2.enableDefault();
    
    tcaselect(6);
    delay(100);
    mag3.init();
    mag3.enableDefault();

    digitalWrite(led2, HIGH);
    
    tcaselect(5);
    delay(100);
    mag4.init();
    mag4.enableDefault();

    tcaselect(4);
    delay(100);
    mag5.init();
    mag5.enableDefault();


    //-----------------Calibration---------------------------------------

    for(int i = 0; i < 20; i++){

              if (i == 5) digitalWrite(led3, HIGH);

              tcaselect(2);
              mag1.read();
              
              tcaselect(3);
              mag2.read();
              
              tcaselect(6);
              mag3.read();
     //was 2         
              tcaselect(5);
              mag4.read();

              tcaselect(4);
              mag5.read();


              m1x = mag1.m.x;
              m1y = mag1.m.y;
              m1z = mag1.m.z;    

              m2x = mag2.m.x;
              m2y = mag2.m.y;
              m2z = mag2.m.z;   

              m3x = mag3.m.x;
              m3y = mag3.m.y;
              m3z = mag3.m.z;   

              m4x = mag4.m.x;
              m4y = mag4.m.y;
              m4z = mag4.m.z;  
              
              m5x = mag5.m.x;
              m5y = mag5.m.y;
              m5z = mag5.m.z;   


 //We add all the aspects of the array and get the average reading. (noww what do we do with this average value?)

              averageM1X += m1x;
              averageM1Y += m1y;
              averageM1Z += m1z;
    
              averageM2X += m2x;
              averageM2Y += m2y;
              averageM2Z += m2z;
    
              averageM3X += m3x;
              averageM3Y += m3y;
              averageM3Z += m3z;
    
              averageM4X += m4x;
              averageM4Y += m4y;
              averageM4Z += m4z;
    
              averageM5X += m5x;
              averageM5Y += m5y;
              averageM5Z += m5z;
    
              delay(20);  
          }

          averageM1X = (averageM1X / 20);
          averageM1Y = (averageM1Y / 20);
          averageM1Z = (averageM1Z / 20);

          averageM2X = (averageM2X / 20);
          averageM2Y = (averageM2Y / 20);
          averageM2Z = (averageM2Z / 20);

          averageM3X = (averageM3X / 20);
          averageM3Y = (averageM3Y / 20); 
          averageM3Z = (averageM3Z / 20);

          averageM4X = (averageM4X / 20);
          averageM4Y = (averageM4Y / 20);
          averageM4Z = (averageM4Z / 20);

          averageM5X = (averageM5X / 20);
          averageM5Y = (averageM5Y / 20);
          averageM5Z = (averageM5Z / 20);
          
          digitalWrite(led1, LOW);
          digitalWrite(led2, LOW);
          digitalWrite(led3, LOW);
          
   
  
  }


void loop(){


  startTime = micros();
 // Serial.println("called");
  
  if (hasHandShaked == false){
    byte readyFlag[1];
    readyFlag[1] = 0x11;
    
     
     Serial.write(readyFlag,1);
     Serial.println("");
     //Serial.println("ready flag, waiting for handshake...");

     if (Serial.available() > 0) {
         //Serial.println("reading serial...");
        char inChar = Serial.read();

        if(inChar){
          
          hasHandShaked = true;
          //start the loop
          //Serial.println("handshake done! now starting to send the data");
          
          }
        }
    }

   else if(hasHandShaked == true){ 
  
 
      String  txtMsg = "_";
      
     while (Serial.available() > 0) {
        //Serial.println("waiting to receive data");
        char inChar = Serial.read();
        txtMsg+= inChar; 
      }
    
      int nu =  txtMsg.length();
      //Serial.println (nu);
         
         if(nu >1 && nu<5){
            shouldStream = true;
         }
         
         else if (nu >5 && nu<10){
          shouldStream = false;  
          hasHandShaked = false;
         }
         
         else {
          shouldStream = false;
         }
    
          if(shouldStream == true){
           //print the last buffer
            Serial.write(fullData,28);
            Serial.println("");
            shouldStream = false;
            Serial.flush();//wait until the end of the print
          }
       
         trackMagnet();
         if (ledTimer < 90)ledLogic();

   }

   
     RoundTripTime = micros() - startTime;
     if(RoundTripTime<4000){
      
      waitTime = (5000-RoundTripTime) ; //depends on baudrate
      delay(waitTime/1000);

      //Serial.println(micros() - startTime);
      
     }

}



/**
 * Callback invoked when scanner pick up an advertising data
 * @param report Structural advertising data
 */
void scan_callback(ble_gap_evt_adv_report_t* report)
{
  //checks
 // Serial.println("scanning");
  static uint8_t xAddressIn[6];
  memcpy( xAddressIn,report->peer_addr.addr, 6);
  //Serial.printBufferReverse(xAddressIn, 6, ':');
 // Serial.println(xAddressIn[0]);

bool isIdentical = false;
  for(int i = 1; i<6; i++){
     if(xAddressIn[i]== xAddress[i]){
         //Serial.println("YO");
         isIdentical = true;
     }
     else {
       isIdentical = false;
      // Serial.println("Wrong peripheral");
      }
     
     }
  
   



 if (isIdentical == true){
  
  // Connect to device with HRM service in advertising
  Bluefruit.Central.connect(report);
  
  }



  

}

/**
 * Callback invoked when an connection is established
 * @param conn_handle
 */
void connect_callback(uint16_t conn_handle)
{
  //Serial.println("Connected");
  //Serial.print("Discovering XRmouse Service ... ");

  // If HRM is not found, disconnect and return
  if ( !hrms.discover(conn_handle) )
  {
 //   Serial.println("Found NONE");

    // disconect since we couldn't find HRM service
    Bluefruit.Central.disconnect(conn_handle);

    return;
  }

  // Once HRM service is found, we continue to discover its characteristic
 // Serial.println("Found it");

  
  //Serial.print("Discovering 3D Positioning characteristic ... ");
  if ( !hrmc.discover() )
  {
    // Measurement chr is mandatory, if it is not found (valid), then disconnect
  //  Serial.println("not found !!!");  
  //  Serial.println("Measurement characteristic is mandatory but not found");
    Bluefruit.Central.disconnect(conn_handle);
    return;
  }
 // Serial.println("Found it");

  // Measurement is found, continue to look for option Body Sensor Location
  // https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.characteristic.body_sensor_location.xml
  // Body Sensor Location is optional, print out the location in text if present
  //Serial.print("Discovering Body Sensor Location characteristic ... ");
  if ( bslc.discover() )
  {
   // Serial.println("Found it");
    
    // Body sensor location value is 8 bit
    const char* body_str[] = { "Other", "Chest", "Wrist", "Finger", "Hand", "Ear Lobe", "Foot" };

    // Read 8-bit BSLC value from peripheral
    uint8_t loc_value = bslc.read8();
    
    ////Serial.print("Body Location Sensor: ");
    //Serial.println(body_str[loc_value]);
  }else
  {
   // Serial.println("Found NONE");
  }

  // Reaching here means we are ready to go, let's enable notification on measurement chr
  if ( hrmc.enableNotify() )
  {
    //Serial.println("Ready to receive HRM Measurement value");
  }else
  {
    //Serial.println("Couldn't enable notify for HRM Measurement. Increase DEBUG LEVEL for troubleshooting");
  }
}

/**
 * Callback invoked when a connection is dropped
 * @param conn_handle
 * @param reason
 */
void disconnect_callback(uint16_t conn_handle, uint8_t reason)
{
  (void) conn_handle;
  (void) reason;
  isCobraConnected = false;

  //Serial.println("Disconnected");
}


/**
 * Hooked callback that triggered when a measurement value is sent from peripheral
 * @param chr   Pointer client characteristic that even occurred,
 *              in this example it should be hrmc
 * @param data  Pointer to received data
 * @param len   Length of received data
 */
void hrm_notify_callback(BLEClientCharacteristic* chr, uint8_t* data, uint16_t len)
{
  // https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.characteristic.heart_rate_measurement.xml
  // Measurement contains of control byte0 and measurement (8 or 16 bit) + optional field
  // if byte0's bit0 is 0 --> measurement is 8 bit, otherwise 16 bit.

//Serial.println(len);
 
  for(unsigned char index=0; index<len; index++) {
    cobraData[index] = data[index];
  }
 isCobraConnected = true;
 
  //Serial.write(cobraData, len);

//  for (int i = 0; i<len;i++){

//    int k = cobraData[i];
 //   Serial.print(k);
 //    Serial.print("-");
    
  //  }
//  Serial.println("");
  //reconstruct the data and print it here rather than in the loop
  //ror

//test for pritning thePady axis
//uint8_t padYmsb = cobraData[19];
//uint8_t padYlsb = cobraData[18];
//uint16_t padY = (padYmsb <<8)|padYlsb;
//Serial.println(padY);
 



  
}

void trackMagnet(){ 
       
         
          tcaselect(2);
          mag1.read();
          m1x = mag1.m.x;
          m1y = mag1.m.y;
          m1z = mag1.m.z;          

          tcaselect(3);
          mag2.read();
          m2x = mag2.m.x;
          m2y = mag2.m.y;
          m2z = mag2.m.z;         
          
          tcaselect(6);
          mag3.read();
          m3x = mag3.m.x;
          m3y = mag3.m.y;
          m3z = mag3.m.z;

 //was 2         
          tcaselect(5);
          mag4.read();
          m4x = mag4.m.x;
          m4y = mag4.m.y;
          m4z = mag4.m.z;
              
          tcaselect(4);
          mag5.read();
          m5x = mag5.m.x;
          m5y = mag5.m.y;
          m5z = mag5.m.z;

          m1x -= averageM1X;
          m1y -= averageM1Y;
          m1z -= averageM1Z;

          m2x -= averageM2X;
          m2y -= averageM2Y;
          m2z -= averageM2Z;

          m3x -= averageM3X;
          m3y -= averageM3Y;
          m3z -= averageM3Z;

          m4x -= averageM4X;
          m4y -= averageM4Y;
          m4z -= averageM4Z;

          m5x -= averageM5X;
          m5y -= averageM5Y;
          m5z -= averageM5Z;


//  String m1xx = String(m1x);
//  String m1yy = String(m1y);
//  String m1zz = String(m1z);
//
//  String m2xx = String(m2x);
//  String m2yy = String(m2y);
//  String m2zz = String(m2z);
//
//  String m3xx = String(m3x);
//  String m3yy = String(m3y);
//  String m3zz = String(m3z);
//  
//  String m4xx = String(m4x);
//  String m4yy = String(m4y);
//  String m4zz = String(m4z);
//
//  String m5xx = String(m5x);
//  String m5yy = String(m5y);
//  String m5zz = String(m5z);
//
//  
//  Serial.println(m1xx + "," + m1yy+ "," + m1zz + ","+ m2xx + "," + m2yy+ "," + m2zz + ","+m3xx + "," + m3yy+ "," + m3zz + ","+m4xx + "," + m4yy+ "," + m4zz + ","+ m5xx + "," + m5yy+ "," + m5zz);

//////////////////////////////////////do all calculations here

            double totalValuesqM1 = sqrt(m1x * m1x + m1y * m1y + m1z * m1z);
            double totalValuesqM2 = sqrt(m2x * m2x + m2y * m2y + m2z * m2z);
            double totalValuesqM3 = sqrt(m3x * m3x + m3y * m3y + m3z * m3z);
            double totalValuesqM4 = sqrt(m4x * m4x + m4y * m4y + m4z * m4z);
            double totalValuesqM5 = sqrt(m5x * m5x + m5y * m5y + m5z * m5z);

//            float totalValuesqM1 = abs(m1x) + abs(m1y)+ abs(m1z);
//            float totalValuesqM2 = abs(m2x) + abs(m2y)+ abs(m2z);
//            float totalValuesqM3 = abs(m3x) + abs(m3y)+ abs(m3z);
//            float totalValuesqM4 = abs(m4x) + abs(m4y)+ abs(m4z);
//            float totalValuesqM5 = abs(m5x) + abs(m5y)+ abs(m5z);
//
//           totalValuesqM1 /= 100;
//           totalValuesqM2 /= 100;
//           totalValuesqM3 /= 100;
//           totalValuesqM4 /= 100;
//           totalValuesqM5 /= 100;
          
  
            
//            double exponent1 = 3.023;
//            double parameter1 = 30205;
            ////Insert expoent selection if here
                        ////Sensor1
            if (totalValuesqM1 >= 4086 )
            {
                exponent1 = 2.69;
                parameter1 = 764788648;
            }
            else if(totalValuesqM1 < 4086)
            {
                exponent1 = 3.04;
                parameter1 = 3745655795;
            }
            if (totalValuesqM2 >= 4086 )
            {
                exponent2 = 2.69;
                parameter2 = 764788648;
            }
            else if (totalValuesqM2 < 4086)
            {
                exponent2 = 3.04f;
                parameter2 = 3745655795;
            }
            if (totalValuesqM3 >= 4086 )
            {
                exponent3 = 2.69;
                parameter3 = 764788648;
            }
            else if (totalValuesqM3 < 4086)
            {
                exponent3 = 3.04;
                parameter3 = 3745655795;
            }
            if (totalValuesqM4 >= 4086 )
            {
                exponent4 = 2.69;
                parameter4 = 764788648;
            }
            else if (totalValuesqM4 < 4086)
            {
                exponent4 = 3.04;
                parameter4 = 3745655795;
            }
            if (totalValuesqM5 >= 4086 )
            {
                exponent5 = 2.69;
                parameter5 = 764788648;
            }
            else if (totalValuesqM5 < 4086)
            {
                exponent5 = 3.04;
                parameter5 = 3745655795;
            }
            zdist1 = (pow((parameter1 / totalValuesqM1), 1 / exponent1));
            zdist2 = (pow((parameter1 / totalValuesqM2), 1 / exponent1));
            zdist3 = (pow((parameter1 / totalValuesqM3), 1 / exponent1));
            zdist4 = (pow((parameter1 / totalValuesqM4), 1 / exponent1));
            zdist5 = (pow((parameter1 / totalValuesqM5), 1 / exponent1));

//              sensperc1 = zdist1;
//              sensperc2 = zdist2;
//              sensperc3 = zdist3;
//              sensperc4 = zdist4;
//              sensperc5 = zdist5;

            float senspercList[5] = {zdist1,zdist2,zdist3,zdist4,zdist5}; // checck if there need to be []
            float senspercList2[5] = {zdist1,zdist2,zdist3,zdist4,zdist5};
  
  //reverting loop variables to start at 0
            maxlistval = 0;
            maxindex = 0;


            for (int j = 0; j < 5; j++)
            {
              for (int i = 0; i < 5; i++)
              {
                if (senspercList[i] >= maxlistval) {
                  maxindex = i;
                  maxlistval = senspercList[i];
                }
              }
              maxvalsindex[j] = maxindex;
              senspercList[maxindex] = 0;
              maxlistval = 0;
            }

              zdist1 = (zdist1 - senspercList2[maxvalsindex[0]]) * -1;
              zdist2 = (zdist2 - senspercList2[maxvalsindex[0]]) * -1;
              zdist3 = (zdist3 - senspercList2[maxvalsindex[0]]) * -1;
              zdist4 = (zdist4 - senspercList2[maxvalsindex[0]]) * -1;
              zdist5 = (zdist5 - senspercList2[maxvalsindex[0]]) * -1;

//  WeightPositionx = (((posweight1x * sensperc1) + (posweight2x * sensperc2) + (posweight3x * sensperc3) + ((posweight4x * sensperc4)) + ((posweight5x * sensperc5)))/1.3);
//  WeightPositiony = (((posweight1y * sensperc1) + (posweight2y * sensperc2) + (posweight3y * sensperc3) + ((posweight4y * sensperc4)) + ((posweight5y * sensperc5)))/1.3);
//  WeightPositionz = (((senspercList2[maxvalsindex[4]] + senspercList2[maxvalsindex[3]])*10) / 2)-60;
  WeightPositionx = (((posweight1x * zdist1) + (posweight2x * zdist2) + (posweight3x * zdist3) + ((posweight4x * zdist4)) + ((posweight5x * zdist5)))/4);
  WeightPositiony = (((posweight1y * zdist1) + (posweight2y * zdist2) + (posweight3y * zdist3) + ((posweight4y * zdist4)) + ((posweight5y * zdist5)))/4);
  WeightPositionz = ((((senspercList2[maxvalsindex[4]] + senspercList2[maxvalsindex[3]])) / 2)-80)*30;

if(starterbool){
  WeightPositionxfirst=WeightPositionx;
  WeightPositionyfirst=WeightPositiony;
  WeightPositionzfirst=WeightPositionz;
  starterbool = false;
  setInitialValues(WeightPositionxfirst,WeightPositionyfirst, WeightPositionzfirst);
}

//first reading - boolean



newX = calcDemaX(WeightPositionx);
newY = calcDemaY(WeightPositiony);
newZ = calcDemaZ(WeightPositionz);

//if (abs(newX) > 45 || abs(newY) > 45 || abs(newZ) > 45 || totalM5 < 300 || max1Total < 300 || max2Total < 300){
if (abs(newX) > 4500 || abs(newY) > 4500 || abs(newZ) > 4500){        
        newX = xPrevious;
        newY = yPrevious;
        newZ = zPrevious;        
      }

      else {
        xPrevious = newX;
        yPrevious = newY;
        zPrevious = newZ;        
      }
      




//  String m1xS = String(newX);
//  String m1yS = String(-1*newY);
//  String m1zS = String(newZ);

//  String m1xS = String(WeightPositionx);
//  String m1yS = String(WeightPositiony);
//  String m1zS = String(WeightPositionz);


////serial print the string
//  Serial.println(m1xS + "," + m1yS+ "," + m1zS + ","+ m2xS + "," + m2yS+ "," + m2zS + ","+m3xS + "," + m3yS+ "," + m3zS + ","+m4xS + "," + m4yS+ "," + m4zS + ","+ m5xS + "," + m5yS+ "," + m5zS);
//    Serial.println(m1xS + "," + m1yS+ "," + m1zS);
//

if (newZ < 3) newZ = 1;

newXint = int16_t(newX);
newYint = int16_t(-1*newY);
newZint = int16_t(newZ);

    byte m1xHigh = (newXint >> 8);
    byte m1xLow = newXint;

    byte m1yHigh = (newYint >> 8);
    byte m1yLow = newYint;

    byte  m1zHigh = (newZint >> 8);
    byte  m1zLow = newZint;

 //this is for only position 
    byte mgData[] = {m1xHigh,m1xLow,m1yHigh,m1yLow,m1zHigh,m1zLow,endOfData};
  //  Serial.write(mgData,7);
   // Serial.println("");

   

    if(isCobraConnected == true){ 
      cobraData[20] = endOfData;
    }
    else{
       cobraData[20] = 0;
      }
 
  for(int i = 0; i<29; i++){
    if(i<7){
      //from [0] to [6] or 7 data points
      fullData[i] = mgData[i];
      
      }
      else{
//from 7 to 27 or 20
         fullData[i] = cobraData[i-7];
        
        }
    
    }
  
 

  }




float emaFunction(float latest, float stored, float alpha){
  return alpha*latest + (1-alpha)*stored;
}

//void setAlpha(float newAlpha){
//  alpha = newAlpha;
//}

float setInitialValues(float x,float y, float z){
   emaX = x;
   emaY = y;
   emaZ = z;

   emaEmaX = x;
   emaEmaY = y;
   emaEmaZ = z;
}

//float calcEmaX(float newX){
//  return emaX = emaFunction(newX, emaX,alpha);
//}

float calcDemaX(float newX){
  emaX = emaFunction(newX, emaX,alpha);
  emaEmaX = emaFunction(emaX, emaEmaX,alpha);
  return 2*emaX - emaEmaX;
}

//float calcEmaY(float newY){
//  return emaY = emaFunction(newY, emaY,alpha);
//}

float calcDemaY(float newY){
  emaY = emaFunction(newY, emaY,alpha);
  emaEmaY = emaFunction(emaY, emaEmaY,alpha);
  return 2*emaY - emaEmaY;
}

//float calcEmaZ(float newZ){
//  return emaZ = emaFunction(newZ, emaZ,alpha);
//}

float calcDemaZ(float newZ){
  emaZ = emaFunction(newZ, emaZ,alpha);
  emaEmaZ = emaFunction(emaZ, emaEmaZ,alpha);
  return 2*emaZ - emaEmaZ;
}






void ledLogic(){
   ledTimer++;

  if (ledTimer == 10){
      digitalWrite(led1, HIGH);      
  }

   if (ledTimer == 20){
      digitalWrite(led2, HIGH);      
  }

   if (ledTimer == 30){
      digitalWrite(led3, HIGH);      
  }

  if (ledTimer == 40){
      digitalWrite(led1, LOW);
      digitalWrite(led2, LOW);
      digitalWrite(led3, LOW);    
  }

  if (ledTimer == 50){
      digitalWrite(led1, HIGH);
      digitalWrite(led2, HIGH);
      digitalWrite(led3, HIGH);    
  }

  if (ledTimer == 60){
      digitalWrite(led1, LOW);
      digitalWrite(led2, LOW);
      digitalWrite(led3, LOW);    
  }

  if (ledTimer == 70){
      digitalWrite(led1, HIGH);
      digitalWrite(led2, HIGH);
      digitalWrite(led3, HIGH);    
  }

  if (ledTimer == 80){
      digitalWrite(led1, LOW);
      digitalWrite(led2, LOW);
      digitalWrite(led3, LOW);    
  }

  if (ledTimer == 90){
      digitalWrite(led1, HIGH);
      digitalWrite(led2, HIGH);
      digitalWrite(led3, HIGH);    
  }

  
  
}
